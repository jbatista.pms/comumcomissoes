import sys
from PySide2 import QtCore, QtWidgets

from QtUtils import subwindowsbase
from servidor.models import Servidor
from .views.excluir import Ui_Dialog
from .models import Cargo


class Excluir(subwindowsbase.Formulario):
    classe_ui = Ui_Dialog

    def inicializar(self, *args, **kwargs):
        self.cargos = [
            i for i in Cargo.select2().where(
                Cargo.id!=self.instance.id
            ).order_by(Cargo.nome)
            ]
        self.ui.cargos.addItems([i.nome for i in self.cargos])

    def salvar(self):
        novo_cargo = self.cargos[self.ui.cargos.currentIndex()]
        for filho in self.instance.servidores.select().where(Servidor.origem.is_null(True)):
            filho.cargo = novo_cargo
            filho.save()
        self.accept()
