from QtUtils import subwindowsbase
from QtUtils.colecoes.dialogos import Alerta
from QtUtils.colecoes.excluir import Excluir
from QtUtils.db import DataBase
from QtUtils.historico import historico
from QtUtils.validadores import TextoEmMaisculo

from servidor.models import Servidor
from .views.cadastro import Ui_Dialog
from .models import Cargo
from . import excluir


class Cadastro(subwindowsbase.Formulario):
    classe_ui = Ui_Dialog

    def accept(self):
        self.instance.nome = self.ui.nome.text()
        self.instance.nivel_escolaridade = self.ui.nivel.currentIndex()
        if not self.instance.nome:
            return Alerta(self,text="Preencha todos os campos!").exec_()

        self.instance.save()
        self.enviar_sinal_atualizacao(self.instance)
        super().accept()

    def excluir(self):
        if Excluir(self).exec_():
            with DataBase().obter_database().atomic():
                servidores = self.instance.servidores.select()
                if servidores:
                    nao_historico = servidores.where(Servidor.origem.is_null(True)).count() > 0
                    if nao_historico and not excluir.Excluir(self, instance=self.instance).exec_():
                        return
                    else:
                        self.instance.excluir_instancia()
                else:
                    self.instance.delete_instance()
                self.enviar_sinal_atualizacao(self.instance)
                self.accept()
    
    def historico(self):
        historico.Historico(self, instance=self.instance).exec_()

    def inicializar(self, *args, **kwargs):
        self.ui.nivel.addItems([v for k,v in Cargo.NIVEL_ESCOLHAS])
        self.ui.nome.setValidator(TextoEmMaisculo(self))
        if self.instance.id:
            self.setWindowTitle("Alterar cadastro de Cargo")
            self.ui.nome.setText(self.instance.nome)
            self.ui.nivel.setCurrentIndex(self.instance.nivel_escolaridade)
            if not self.instance.data_exclusao is None:
                self.ui.excluir.setEnabled(False)
                self.ui.salvar.setEnabled(False)
                self.ui.nome.setEnabled(False)
                self.ui.nivel.setEnabled(False)
        else:
            self.ui.historico.hide()
            self.ui.excluir.hide()

    def instanciar(self, *args, **kwargs):
        return kwargs.get('instance', Cargo())