# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'listar.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(492, 644)
        Form.setMinimumSize(QSize(492, 644))
        Form.setMaximumSize(QSize(16777215, 16777215))
        self.gridLayout = QGridLayout(Form)
        self.gridLayout.setObjectName(u"gridLayout")
        self.frame = QFrame(Form)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.frame)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.pushButton = QPushButton(self.frame)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout.addWidget(self.pushButton)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)


        self.gridLayout.addWidget(self.frame, 3, 0, 1, 3)

        self.label = QLabel(Form)
        self.label.setObjectName(u"label")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)

        self.gridLayout.addWidget(self.label, 1, 0, 1, 1)

        self.pesquisa = QLineEdit(Form)
        self.pesquisa.setObjectName(u"pesquisa")

        self.gridLayout.addWidget(self.pesquisa, 1, 1, 1, 1)

        self.tabela = QTableWidget(Form)
        if (self.tabela.columnCount() < 2):
            self.tabela.setColumnCount(2)
        __qtablewidgetitem = QTableWidgetItem()
        self.tabela.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.tabela.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        self.tabela.setObjectName(u"tabela")
        self.tabela.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.tabela.setAlternatingRowColors(True)
        self.tabela.setSelectionMode(QAbstractItemView.SingleSelection)
        self.tabela.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tabela.horizontalHeader().setHighlightSections(False)
        self.tabela.horizontalHeader().setStretchLastSection(True)
        self.tabela.verticalHeader().setVisible(False)

        self.gridLayout.addWidget(self.tabela, 2, 0, 1, 3)

        self.excluidos = QCheckBox(Form)
        self.excluidos.setObjectName(u"excluidos")

        self.gridLayout.addWidget(self.excluidos, 1, 2, 1, 1)

        QWidget.setTabOrder(self.pesquisa, self.excluidos)
        QWidget.setTabOrder(self.excluidos, self.tabela)

        self.retranslateUi(Form)
        self.pesquisa.textChanged.connect(Form.atualizar)
        self.tabela.cellDoubleClicked.connect(Form.editar)
        self.excluidos.stateChanged.connect(Form.atualizar)
        self.pushButton.released.connect(Form.close)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Cargos", None))
        self.pushButton.setText(QCoreApplication.translate("Form", u"Fechar", None))
        self.label.setText(QCoreApplication.translate("Form", u"Pesquisa:", None))
        ___qtablewidgetitem = self.tabela.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("Form", u"id", None));
        ___qtablewidgetitem1 = self.tabela.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("Form", u"Nome", None));
        self.excluidos.setText(QCoreApplication.translate("Form", u"Exclu\u00eddos", None))
    # retranslateUi

