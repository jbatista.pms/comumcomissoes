# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'cadastro.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(457, 163)
        Dialog.setMinimumSize(QSize(457, 163))
        Dialog.setMaximumSize(QSize(457, 163))
        self.gridLayout_2 = QGridLayout(Dialog)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.nivel = QComboBox(Dialog)
        self.nivel.setObjectName(u"nivel")

        self.gridLayout_2.addWidget(self.nivel, 3, 0, 1, 2)

        self.frame = QFrame(Dialog)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.gridLayout = QGridLayout(self.frame)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName(u"gridLayout")
        self.salvar = QPushButton(self.frame)
        self.salvar.setObjectName(u"salvar")

        self.gridLayout.addWidget(self.salvar, 0, 1, 1, 1)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_2, 0, 5, 1, 1)

        self.excluir = QPushButton(self.frame)
        self.excluir.setObjectName(u"excluir")

        self.gridLayout.addWidget(self.excluir, 0, 3, 1, 1)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer, 0, 0, 1, 1)

        self.pushButton_2 = QPushButton(self.frame)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.gridLayout.addWidget(self.pushButton_2, 0, 2, 1, 1)

        self.historico = QPushButton(self.frame)
        self.historico.setObjectName(u"historico")

        self.gridLayout.addWidget(self.historico, 0, 4, 1, 1)


        self.gridLayout_2.addWidget(self.frame, 5, 0, 1, 2)

        self.label_2 = QLabel(Dialog)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_2, 2, 0, 1, 1)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_2.addItem(self.verticalSpacer, 4, 0, 1, 1)

        self.label = QLabel(Dialog)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label, 0, 0, 1, 1)

        self.nome = QLineEdit(Dialog)
        self.nome.setObjectName(u"nome")

        self.gridLayout_2.addWidget(self.nome, 1, 0, 1, 2)

        QWidget.setTabOrder(self.nome, self.nivel)
        QWidget.setTabOrder(self.nivel, self.salvar)
        QWidget.setTabOrder(self.salvar, self.pushButton_2)
        QWidget.setTabOrder(self.pushButton_2, self.excluir)
        QWidget.setTabOrder(self.excluir, self.historico)

        self.retranslateUi(Dialog)
        self.salvar.released.connect(Dialog.accept)
        self.pushButton_2.released.connect(Dialog.reject)
        self.excluir.released.connect(Dialog.excluir)
        self.historico.released.connect(Dialog.historico)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Cadastro de Cargo", None))
        self.salvar.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
        self.excluir.setText(QCoreApplication.translate("Dialog", u"Excluir", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog", u"Fechar", None))
        self.historico.setText(QCoreApplication.translate("Dialog", u"Hist\u00f3rico", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"N\u00edvel de escolaridade:", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Nome:", None))
    # retranslateUi

