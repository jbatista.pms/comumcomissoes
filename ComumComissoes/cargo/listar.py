import sys
from PySide2 import QtWidgets

from QtUtils import subwindowsbase
from QtUtils.validadores import TextoEmMaisculo

from . import cadastro
from .models import Cargo
from .views.listar import Ui_Form


class Control(subwindowsbase.Listar):
    classe_ui = Ui_Form

    def inicializar(self, *args, **kwargs):
        self.ui.pesquisa.setValidator(TextoEmMaisculo(self))
        self.ui.pesquisa.setFocus()
        self.atualizar()
        self.ui.tabela.horizontalHeader().setSectionHidden(0,True)

    def atualizar(self):
        cargos = Cargo.select2()
        pesquisa = self.ui.pesquisa.text()
        excluidos = self.ui.excluidos.isChecked()
        if excluidos:
            cargos = Cargo.select().where(
                (Cargo.origem.is_null(True)) &
                (Cargo.data_exclusao.is_null(False))
            ).order_by(Cargo.nome)
        if pesquisa:
            cargos = cargos.where(Cargo.nome.contains(pesquisa))
        qtd = cargos.count()
        self.ui.tabela.setRowCount(qtd)
        for f in range(qtd):
            self.ui.tabela.setItem(f,0, QtWidgets.QTableWidgetItem())
            self.ui.tabela.setItem(f,1, QtWidgets.QTableWidgetItem())
            self.ui.tabela.item(f,0).setText(str(cargos[f].id))
            self.ui.tabela.item(f,1).setText(cargos[f].nome)
        self.lista_dict = {str(c.id):c for c in cargos}

    def editar(self, row, column):
        id_cargo = self.ui.tabela.item(row,0).text()
        if cadastro.Cadastro(self, instance=self.lista_dict[id_cargo]).exec_():
            self.atualizar()

    def receber_sinal_atualizacao(self, instance):
        if type(instance) == Cargo:
            self.atualizar()
