from PySide2 import QtWidgets

from QtUtils import subwindowsbase
from QtUtils.validadores import TextoEmMaisculo

from .cadastro import Cadastro
from .models import Lotacao
from .views.listar import Ui_Form


class Control(subwindowsbase.Listar):
    classe_ui = Ui_Form

    def inicializar(self, *args, **kwargs):
        self.ui.pesquisa.setValidator(TextoEmMaisculo(self))
        self.ui.pesquisa.setFocus()
        self.atualizar()

    def atualizar(self, qstring=None):
        lotacoes = Lotacao.select2(
            excluidos=self.ui.excluidos.isChecked()
        ).where(Lotacao.pai==None)
        if qstring:
            lotacoes = lotacoes.where(
                Lotacao.nome.contains(self.ui.pesquisa.text())
                )
        self.lista_dict = {}
        self.ui.tabela.clear()
        for lot in lotacoes:
            pai = QtWidgets.QTreeWidgetItem(self.ui.tabela)
            pai.setText(0, lot.nome)
            pai.setText(1, str(lot.id))
            sub_lot = lot.filhos.select().where(
                (Lotacao.data_exclusao.is_null(True)) &
                (Lotacao.origem.is_null(True))
            ).order_by(Lotacao.nome)
            for filho in sub_lot:
                f = QtWidgets.QTreeWidgetItem(pai)
                f.setText(0, filho.nome)
                f.setText(1, str(filho.id))
                self.lista_dict.update({str(filho.id):filho})
        self.lista_dict.update({str(l.id):l for l in lotacoes})

    def editar(self, item, coluna):
        Cadastro(self, instance=self.lista_dict[item.text(1)]).exec_()

    def criar(self):
        Cadastro(self).exec_()
    
    def receber_sinal_atualizacao(self, instance):
        if isinstance(instance, Lotacao):
            self.atualizar()
