from PySide2 import QtCore
from .models import Lotacao


class LotacaoSelecao(object):
    SL_LOTACAO = 0
    SL_SUBLOTACAO = 1
    SL_LOTACAO_SUBLOTACAO = 2

    selecao_busca = True
    sublot_padroes = 2
    __sublotacoes = []
    __lotacoes = []

    def contruir(self, *args, **kwargs):
        QtCore.QObject.connect(
            self.ui.lotacao,
            QtCore.SIGNAL("currentIndexChanged(int)"),
            self.carregar_sublotacao
        )
        if hasattr(super(), 'contruir'):
            super().contruir(*args, **kwargs)

    def carregar_lotacoes(self, lotacoes=None):
        self.__sublot_padroes = 2 if self.selecao_busca else 1
        self.ui.lotacao.clear()
        if lotacoes:
            self.__lotacoes = list(lotacoes)
        else:
            self.__lotacoes = list(
                Lotacao.select2().where(Lotacao.pai==None)
            )
        self.ui.lotacao.addItem("")
        self.ui.lotacao.addItems([i.nome for i in self.__lotacoes])

    def carregar_sublotacao(self):
        self.ui.sublotacao.clear()
        lotacao = self.lotacao_selecionada()
        if lotacao!=None:
            self.__sublotacoes = list(
                lotacao.filhos.select().where(
                    (Lotacao.data_exclusao.is_null(True)) &
                    (Lotacao.origem.is_null(True))
                ).order_by(Lotacao.nome)
            )
            if self.__sublotacoes:
                if self.selecao_busca:
                    self.ui.sublotacao.addItem('Todos')
                    self.ui.sublotacao.addItem('Somente')
                else:
                    self.ui.sublotacao.addItem('Nenhum')
                self.ui.sublotacao.addItems(
                    [i.nome for i in self.__sublotacoes]
                )

    def limpar_lotacao(self):
        self.ui.lotacao.setCurrentIndex(0)

    def limpar_sublotacao(self):
        self.ui.sublotacao.setCurrentIndex(0)

    def lotacao_indice(self):
        return self.ui.lotacao.currentIndex()

    def lotacao_selecionada(self):
        if self.__lotacoes and self.lotacao_indice()>0:
            return self.__lotacoes[self.lotacao_indice()-1]
        return None

    def locatao_resultado(self):
        return self.sublotacao_selecionada() or self.lotacao_selecionada()
    
    def selecao(self):
        lotacao_index = self.lotacao_indice()
        sublotacao_index = self.sublotacao_indice()
        if lotacao_index:
            if self.selecao_busca:
                if sublotacao_index == 0:
                    return self.SL_LOTACAO_SUBLOTACAO, self.lotacao_selecionada()
                elif sublotacao_index == 1:
                    return self.SL_LOTACAO, self.lotacao_selecionada()
                else:
                    return self.SL_SUBLOTACAO, self.sublotacao_selecionada()
            else:
                if sublotacao_index == 0:
                    return self.SL_LOTACAO, self.lotacao_selecionada()
                else:
                    return self.SL_SUBLOTACAO, self.sublotacao_selecionada()
        return None, None

    def selecionar_lotacao(self, lotacao):
        if not self.__lotacoes:
            self.carregar_lotacoes()
        if lotacao in self.__lotacoes:
            self.ui.lotacao.setCurrentIndex(
                self.__lotacoes.index(lotacao)+1
            )
            self.carregar_sublotacao()
        elif lotacao.pai:
            if lotacao.pai in self.__lotacoes:
                self.ui.lotacao.setCurrentIndex(
                    self.__lotacoes.index(lotacao.pai)+1
                )
                self.carregar_sublotacao()
                if lotacao in self.__sublotacoes:
                    self.ui.sublotacao.setCurrentIndex(
                        self.__sublotacoes.index(lotacao)+self.__sublot_padroes
                    )

    def sublotacao_indice(self):
        return self.ui.sublotacao.currentIndex()

    def sublotacao_selecionada(self):
        if self.__sublotacoes and self.sublotacao_indice()>self.__sublot_padroes-1:
            return self.__sublotacoes[self.sublotacao_indice()-self.__sublot_padroes]
        return None
