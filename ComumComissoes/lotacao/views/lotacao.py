# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'lotacao.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(431, 163)
        Dialog.setMinimumSize(QSize(431, 163))
        Dialog.setMaximumSize(QSize(431, 163))
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label = QLabel(Dialog)
        self.label.setObjectName(u"label")

        self.verticalLayout.addWidget(self.label)

        self.nome = QLineEdit(Dialog)
        self.nome.setObjectName(u"nome")

        self.verticalLayout.addWidget(self.nome)

        self.label_2 = QLabel(Dialog)
        self.label_2.setObjectName(u"label_2")

        self.verticalLayout.addWidget(self.label_2)

        self.lotacoes = QComboBox(Dialog)
        self.lotacoes.setObjectName(u"lotacoes")
        self.lotacoes.setEnabled(True)

        self.verticalLayout.addWidget(self.lotacoes)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)

        self.frame = QFrame(Dialog)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.frame)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.salvar = QPushButton(self.frame)
        self.salvar.setObjectName(u"salvar")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.salvar.sizePolicy().hasHeightForWidth())
        self.salvar.setSizePolicy(sizePolicy)

        self.horizontalLayout.addWidget(self.salvar)

        self.fechar = QPushButton(self.frame)
        self.fechar.setObjectName(u"fechar")
        sizePolicy.setHeightForWidth(self.fechar.sizePolicy().hasHeightForWidth())
        self.fechar.setSizePolicy(sizePolicy)

        self.horizontalLayout.addWidget(self.fechar)

        self.excluir = QPushButton(self.frame)
        self.excluir.setObjectName(u"excluir")

        self.horizontalLayout.addWidget(self.excluir)

        self.historico = QPushButton(self.frame)
        self.historico.setObjectName(u"historico")

        self.horizontalLayout.addWidget(self.historico)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)


        self.verticalLayout.addWidget(self.frame)

        QWidget.setTabOrder(self.nome, self.lotacoes)
        QWidget.setTabOrder(self.lotacoes, self.salvar)
        QWidget.setTabOrder(self.salvar, self.fechar)

        self.retranslateUi(Dialog)
        self.fechar.released.connect(Dialog.reject)
        self.salvar.released.connect(Dialog.salvar)
        self.excluir.released.connect(Dialog.excluir)
        self.historico.released.connect(Dialog.historico)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Criar lota\u00e7\u00e3o", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Nome:", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Sublota\u00e7\u00e3o de:", None))
        self.salvar.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
        self.fechar.setText(QCoreApplication.translate("Dialog", u"Fechar", None))
        self.excluir.setText(QCoreApplication.translate("Dialog", u"Excluir", None))
        self.historico.setText(QCoreApplication.translate("Dialog", u"Hist\u00f3rico", None))
    # retranslateUi

