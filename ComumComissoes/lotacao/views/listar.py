# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'listar.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(433, 498)
        Form.setMinimumSize(QSize(433, 498))
        self.gridLayout = QGridLayout(Form)
        self.gridLayout.setObjectName(u"gridLayout")
        self.label = QLabel(Form)
        self.label.setObjectName(u"label")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)

        self.gridLayout.addWidget(self.label, 1, 0, 1, 1)

        self.tabela = QTreeWidget(Form)
        __qtreewidgetitem = QTreeWidgetItem()
        __qtreewidgetitem.setText(0, u"1");
        self.tabela.setHeaderItem(__qtreewidgetitem)
        self.tabela.setObjectName(u"tabela")
        self.tabela.setAlternatingRowColors(True)
        self.tabela.setAnimated(True)
        self.tabela.setHeaderHidden(True)

        self.gridLayout.addWidget(self.tabela, 2, 0, 1, 3)

        self.pesquisa = QLineEdit(Form)
        self.pesquisa.setObjectName(u"pesquisa")

        self.gridLayout.addWidget(self.pesquisa, 1, 1, 1, 1)

        self.excluidos = QCheckBox(Form)
        self.excluidos.setObjectName(u"excluidos")

        self.gridLayout.addWidget(self.excluidos, 1, 2, 1, 1)

        self.frame = QFrame(Form)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.frame)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.fechar = QPushButton(self.frame)
        self.fechar.setObjectName(u"fechar")

        self.horizontalLayout.addWidget(self.fechar)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)


        self.gridLayout.addWidget(self.frame, 3, 0, 1, 3)

        QWidget.setTabOrder(self.pesquisa, self.tabela)

        self.retranslateUi(Form)
        self.tabela.itemDoubleClicked.connect(Form.editar)
        self.pesquisa.textChanged.connect(Form.atualizar)
        self.excluidos.released.connect(Form.atualizar)
        self.fechar.released.connect(Form.close)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Lota\u00e7\u00f5es", None))
        self.label.setText(QCoreApplication.translate("Form", u"Pesquisa:", None))
        self.excluidos.setText(QCoreApplication.translate("Form", u"Exclu\u00eddos", None))
        self.fechar.setText(QCoreApplication.translate("Form", u"Fechar", None))
    # retranslateUi

