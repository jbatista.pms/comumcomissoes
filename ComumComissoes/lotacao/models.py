from peewee import *
from QtUtils.historico.models import Historico


class Lotacao(Historico):
    nome = CharField()
    origem = ForeignKeyField('self', related_name='historico', null=True, on_delete='CASCADE')
    pai = ForeignKeyField('self', related_name='filhos', null=True)
    
    def __repr__(self):
        return '<Lotação %s>' % self.nome

    def __str__(self):
        return self.nome
        
    def exportar_extras():
        return ['pai',]

    @property
    def descricao(self):
        if self.pai:
            return "%s - %s" % (self.pai, self.nome)
        return self.nome
    
    def select2(excluidos=False, *args, **kwargs):
        return Lotacao.select(*args, **kwargs).where(
            (Lotacao.data_exclusao.is_null(not excluidos)) &
            (Lotacao.origem.is_null(True))
        ).order_by(Lotacao.nome)