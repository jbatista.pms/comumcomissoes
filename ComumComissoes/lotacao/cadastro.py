from QtUtils import subwindowsbase
from QtUtils.colecoes.excluir import Excluir
from QtUtils.db import DataBase
from QtUtils.historico import historico
from QtUtils.validadores import TextoEmMaisculo

from servidor.models import Servidor
from .views.lotacao import Ui_Dialog
from .models import Lotacao
from . import excluir


class Cadastro(subwindowsbase.Formulario):
    classe_ui = Ui_Dialog

    def carregar_lotacoes(self):
        self.lotacoes = list(
            Lotacao.select2().where(
                (Lotacao.pai==None) & (Lotacao.id!=self.instance.id)
            )
        )
        self.ui.lotacoes.addItem("Nenhum")
        self.ui.lotacoes.addItems([i.nome for i in self.lotacoes])
        if self.instance.pai:
            self.ui.lotacoes.setCurrentIndex(
                self.lotacoes.index(self.instance.pai)+1
                )

    def excluir(self):
        if Excluir(self).exec_():
            with DataBase().obter_database().atomic():
                servidores = self.instance.servidores.select()
                if servidores:
                    nao_historico = servidores.where(Servidor.origem.is_null(True)).count() > 0
                    if nao_historico and not excluir.Control(self, instance=self.instance).exec_():
                        return
                    else:
                        self.instance.pai = None
                        self.instance.excluir_instancia()
                else:
                    self.instance.delete_instance()
                self.enviar_sinal_atualizacao(self.instance)
                self.accept()
    
    def historico(self):
        historico.Historico(self, instance=self.instance).exec_()

    def inicializar(self, *args, **kwargs):
        if self.instance.id:
            self.setWindowTitle(self.instance.nome)
            self.ui.nome.setValidator(TextoEmMaisculo(self))
            self.ui.nome.setText(self.instance.nome)
            if self.instance.data_exclusao is None:
                if self.instance.filhos.select():
                    self.ui.lotacoes.setEnabled(False)
                    self.ui.excluir.setEnabled(False)
                else:
                    self.carregar_lotacoes()
            else:
                self.ui.excluir.setEnabled(False)
                self.ui.lotacoes.setEnabled(False)
                self.ui.nome.setEnabled(False)
                self.ui.salvar.setEnabled(False)
        else:
            self.ui.nome.setValidator(TextoEmMaisculo(self))
            self.carregar_lotacoes()
            self.ui.excluir.hide()
            self.ui.historico.hide()

    def instanciar(self, *args, **kwargs):
        return kwargs.get('instance', Lotacao())

    def teste_unica(self, windows):
        if windows.instance.id == self.instance.id:
            return True
        return False

    def salvar(self):
        self.instance.nome = self.ui.nome.text()
        lotacoes_index = self.ui.lotacoes.currentIndex()
        if lotacoes_index > 0:
            self.instance.pai = self.lotacoes[lotacoes_index-1]
        else:
            self.instance.pai = None
        self.instance.save()
        self.enviar_sinal_atualizacao(self.instance)
        self.accept()