from QtUtils import subwindowsbase
from servidor.models import Servidor
from .views.excluir import Ui_Dialog
from .models import Lotacao
from .controles_uteis import LotacaoSelecao


class Control(subwindowsbase.Formulario, LotacaoSelecao):
    classe_ui = Ui_Dialog
    selecao_busca = False

    def inicializar(self, *args, **kwargs):
        self.carregar_lotacoes()

    def salvar(self):
        nova_lotacao = self.locatao_resultado()
        for servidor in self.instance.servidores.select().where(Servidor.origem.is_null(True)):
            servidor.lotacao = nova_lotacao
            servidor.save()
        self.accept()
